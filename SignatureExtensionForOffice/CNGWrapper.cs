﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SignatureExtensionForOffice
{
    

    #region Structures
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct CRYPT_KEY_PROV_INFO
    {
        [MarshalAs(UnmanagedType.LPWStr)]
        public string pwszContainerName;
        [MarshalAs(UnmanagedType.LPWStr)]
        public string pwszProvName;
        public uint dwProvType;
        public uint dwFlags;
        public uint cProvParam;
        public IntPtr rgProvParam;
        public uint dwKeySpec;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct CRYPTOAPI_BLOB
    {
        public uint cbData;
        public IntPtr pbData;
    }
    #endregion

    class CNGWrapper
    {
        public const string BCRYPT_HASH_LENGTH = "HashDigestLength";
        public const string BCRYPT_OBJECT_LENGTH = "ObjectLength";
        public const string BCRYPT_ECCPUBLIC_BLOB = "ECCPUBLICBLOB";
        public const uint BIGN_PUBLIC_CURVE256_MAGIC = 0x50553536;
        public const uint BIGN_PUBLIC_CURVE384_MAGIC = 0x50553834;
        public const uint BIGN_PUBLIC_CURVE512_MAGIC = 0x50553132;
        public const uint STB1176_PUBLIC_PARAM3_MAGIC = 0x53505533;
        public const uint STB1176_PUBLIC_PARAM6_MAGIC = 0x53505536;
        public const uint STB1176_PUBLIC_PARAM10_MAGIC = 0x53505558;
        public const uint STB1176_PRE_PUBLIC_PARAM3_MAGIC = 0x50505533;
        public const uint STB1176_PRE_PUBLIC_PARAM6_MAGIC = 0x50505536;
        public const uint STB1176_PRE_PUBLIC_PARAM10_MAGIC = 0x50505558;

        [DllImport("crypt32.dll")]
        public static extern bool CertGetCertificateContextProperty(IntPtr pCertContext, uint dwPropId, IntPtr pvData, ref uint pcbData);

        [DllImport("Ncrypt.dll", SetLastError = true, ExactSpelling = true)]
        public static extern int NCryptOpenStorageProvider(out IntPtr hProvider, [MarshalAs(UnmanagedType.LPWStr)] string szProviderName, int flags);

        [DllImport("Ncrypt.dll", SetLastError = true, ExactSpelling = true)]
        public static extern int NCryptOpenKey(IntPtr hProvider,
                                                 out IntPtr hKey,
                                                 [MarshalAs(UnmanagedType.LPWStr)] string szKeyName,
                                                 int legacykeyspec,
                                                 int flags);

        [DllImport("NCrypt.dll", SetLastError = true, ExactSpelling = true)]
        public static extern int NCryptSignHash(IntPtr hKey,
            IntPtr pPaddingInfo,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pbHashValue,
            int cbHashValue,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pbSignature,
            int cbSignature,
            ref int pcbResult,
            int dwFlags);

        [DllImport("Ncrypt.dll", SetLastError = true, ExactSpelling = true)]
        public static extern int NCryptVerifySignature(IntPtr hKey,
            IntPtr pPaddingInfo,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pbHashValue,
            int cbHashValue,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pbSignature,
            int cbSignature,
            int dwFlags);

        [DllImport("Ncrypt.dll", SetLastError = true, ExactSpelling = true)]
        public static extern int NCryptFreeObject(IntPtr hObject);

        [DllImport("Bcrypt.dll", SetLastError = true, ExactSpelling = true)]
        public static extern int BCryptOpenAlgorithmProvider(out IntPtr hAlgorithm,
            [MarshalAs(UnmanagedType.LPWStr)] string pszAlgId,
            [MarshalAs(UnmanagedType.LPWStr)] string pszImplementation,
            int dwFlags);

        [DllImport("Bcrypt.dll", SetLastError = true, ExactSpelling = true)]
        public static extern int BCryptCreateHash(IntPtr hAlgorithm,
            out IntPtr hHash,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pbHashObject,
            int cbHashObject,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pbSecret,
            int cbSecret,
            int dwFlags);

        [DllImport("Bcrypt.dll", SetLastError = true, ExactSpelling = true)]
        public static extern int BCryptHashData(IntPtr hHash,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pbInput,
            int cbInput,
            int dwFlags);

        [DllImport("Bcrypt.dll", SetLastError = true, ExactSpelling = true)]
        public static extern int BCryptFinishHash(IntPtr hHash,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pbOutput,
            int cbOutput,
            int dwFlags);

        [DllImport("Bcrypt.dll", SetLastError = true, ExactSpelling = true)]
        public static extern int BCryptDestroyHash(IntPtr hHash);

        [DllImport("Bcrypt.dll", SetLastError = true, ExactSpelling = true)]
        public static extern int BCryptCloseAlgorithmProvider(IntPtr hAlgorithm, int dwFlags);

        [DllImport("Bcrypt.dll", SetLastError = true, ExactSpelling = true)]
        public static extern int BCryptGetProperty(IntPtr hObject,
            [MarshalAs(UnmanagedType.LPWStr)] string pszProperty,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pboutput,
            int cbOutput,
            ref int pcbResult,
            int dwFlags);

        [DllImport("Bcrypt.dll", SetLastError = true, ExactSpelling = true)]
        public static extern int BCryptVerifySignature(IntPtr hKey,
            IntPtr pPaddingInfo,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pbHash,
            int cbHash,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pbSignature,
            int cbSignature,
            int dwFlags);

        [DllImport("Bcrypt.dll", SetLastError = true, ExactSpelling = true)]
        public static extern int BCryptImportKeyPair(IntPtr hAlgorithm,
            IntPtr hImportKey,
            [MarshalAs(UnmanagedType.LPWStr)] string pszBlobType,
            out IntPtr phKey,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pbInput,
            int cbInput,
            int dwFlags);

        [DllImport("BCrypt.dll", SetLastError = true, ExactSpelling = true)]
        public static extern int BCryptDestroyKey(IntPtr hKey);
    }
}
