namespace SignatureExtensionForOffice
{
	using System;
	using Extensibility;
	using System.Runtime.InteropServices;
    using Microsoft.Office.Core;
    using Microsoft.Office.Interop;
    using System.Reflection;
    using System.Windows.Forms;
    using System.Security.Cryptography;
    using System.Security.Cryptography.Xml;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;

	#region Read me for Add-in installation and setup information.
	// When run, the Add-in wizard prepared the registry for the Add-in.
	// At a later time, if the Add-in becomes unavailable for reasons such as:
	//   1) You moved this project to a computer other than which is was originally created on.
	//   2) You chose 'Yes' when presented with a message asking if you wish to remove the Add-in.
	//   3) Registry corruption.
	// you will need to re-register the Add-in by building the SignatureExtensionForOfficeSetup project, 
	// right click the project in the Solution Explorer, then choose install.
	#endregion
	
	/// <summary>
	///   The object for implementing an Add-in.
	/// </summary>
	/// <seealso class='IDTExtensibility2' />
	[GuidAttribute("EA962C80-D41D-4382-A600-894FA93DE73F"), ProgId("SignatureExtensionForOffice.Connect")]
	public class Connect : Object, Extensibility.IDTExtensibility2, SignatureProvider, IRibbonExtensibility
	{
        private struct OfficeApp
        {
            public string AppType;
            public string AppVersion;
        }

        private OfficeApp officeApp;
        private object applicationObject;
        private object addInInstance;
        public virtual string HashAlgorithmIdentifier { get { return SignedXml.XmlDsigSHA1Url; } }
        public virtual string ProviderUrl { get { return "http://www.niitzi.by"; } }

        private X509Certificate2 m_cert;
        private StringBuilder m_errors;
        private SignatureInfo m_sigInfo;
        private object m_parentWindow;

		/// <summary>
		///		Implements the constructor for the Add-in object.
		///		Place your initialization code within this method.
		/// </summary>
		public Connect()
		{
		}

		/// <summary>
		///      Implements the OnConnection method of the IDTExtensibility2 interface.
		///      Receives notification that the Add-in is being loaded.
		/// </summary>
		/// <param term='application'>
		///      Root object of the host application.
		/// </param>
		/// <param term='connectMode'>
		///      Describes how the Add-in is being loaded.
		/// </param>
		/// <param term='addInInst'>
		///      Object representing this Add-in.
		/// </param>
		/// <seealso class='IDTExtensibility2' />
		public void OnConnection(object application, Extensibility.ext_ConnectMode connectMode, object addInInst, ref System.Array custom)
		{
			applicationObject = application;
			addInInstance = addInInst;
            System.Windows.Forms.Application.EnableVisualStyles();
		}

		/// <summary>
		///     Implements the OnDisconnection method of the IDTExtensibility2 interface.
		///     Receives notification that the Add-in is being unloaded.
		/// </summary>
		/// <param term='disconnectMode'>
		///      Describes how the Add-in is being unloaded.
		/// </param>
		/// <param term='custom'>
		///      Array of parameters that are host application specific.
		/// </param>
		/// <seealso class='IDTExtensibility2' />
		public void OnDisconnection(Extensibility.ext_DisconnectMode disconnectMode, ref System.Array custom)
		{
		}

		/// <summary>
		///      Implements the OnAddInsUpdate method of the IDTExtensibility2 interface.
		///      Receives notification that the collection of Add-ins has changed.
		/// </summary>
		/// <param term='custom'>
		///      Array of parameters that are host application specific.
		/// </param>
		/// <seealso class='IDTExtensibility2' />
		public void OnAddInsUpdate(ref System.Array custom)
		{
		}

		/// <summary>
		///      Implements the OnStartupComplete method of the IDTExtensibility2 interface.
		///      Receives notification that the host application has completed loading.
		/// </summary>
		/// <param term='custom'>
		///      Array of parameters that are host application specific.
		/// </param>
		/// <seealso class='IDTExtensibility2' />
		public void OnStartupComplete(ref System.Array custom)
		{
		}

		/// <summary>
		///      Implements the OnBeginShutdown method of the IDTExtensibility2 interface.
		///      Receives notification that the host application is being unloaded.
		/// </summary>
		/// <param term='custom'>
		///      Array of parameters that are host application specific.
		/// </param>
		/// <seealso class='IDTExtensibility2' />
		public void OnBeginShutdown(ref System.Array custom)
		{
		}
		


        public stdole.IPictureDisp GenerateSignatureLineImage(SignatureLineImage siglnimg, SignatureSetup psigsetup, SignatureInfo psiginfo, object XmlDsigStream)
        {
            throw new NotImplementedException();
        }

        public dynamic GetProviderDetail(SignatureProviderDetail sigprovdet)
        {
            switch (sigprovdet)
            {
                case Microsoft.Office.Core.SignatureProviderDetail.sigprovdetHashAlgorithm:
                    return HashAlgorithmIdentifier;

                case Microsoft.Office.Core.SignatureProviderDetail.sigprovdetUIOnly:
                    return false;

                case Microsoft.Office.Core.SignatureProviderDetail.sigprovdetUrl:
                    return this.ProviderUrl;

                default:
                    return null;
            }
        }

        public Array HashStream(object QueryContinue, object Stream)
        {
            MessageBox.Show("HashStream");

            using (COMStream comstream = new COMStream(Stream))
            {
                //byte[] buffer = new byte[comstream.Length];
                //comstream.Read(buffer, 0, (int)comstream.Length);
                ////MessageBox.Show(Encoding.Default.GetString(buffer));
                //byte[] bDigest = new byte[32];
                //tokenHandle.MakeDigest(buffer, bDigest);
                //return bDigest;
                using (HashAlgorithm hashalg = HashAlgorithm.Create("SHA1"))
                {
                    return hashalg.ComputeHash(comstream);
                }
            }
        }

        public void NotifySignatureAdded(object ParentWindow, SignatureSetup psigsetup, SignatureInfo psiginfo)
        {
            MessageBox.Show("�������� ������� ��������", "����������", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void ShowSignatureDetails(object ParentWindow, SignatureSetup psigsetup, SignatureInfo psiginfo, object XmlDsigStream, ref ContentVerificationResults pcontverres, ref CertificateVerificationResults pcertverres)
        {
            SignInfo form = new SignInfo();
            form.MaximizeBox = false;
            form.FormBorderStyle = FormBorderStyle.Fixed3D;
            form.StartPosition = FormStartPosition.CenterParent;
            form.Text = "����������";
            form.ViewCertClicked += new SignInfo.Handler(form_ViewCertClicked);
            form.AdditionalInfoClicked += new SignInfo.Handler(form_AdditionalInfoClicked);
            m_sigInfo = psiginfo;
            m_parentWindow = ParentWindow;

            string certSubjectName = (string)psiginfo.GetCertificateDetail(CertificateDetail.certdetSubject);

            DateTime dt = psiginfo.GetSignatureDetail(SignatureDetail.sigdetLocalSigningTime);
            form.SetDateLabel(dt.ToShortDateString());

            if (pcontverres == ContentVerificationResults.contverresValid && pcertverres == CertificateVerificationResults.certverresValid)
            {
                form.SetSignerNameLabel(certSubjectName);
                form.SetSignStatusLabel("������� �������������.", System.Drawing.Color.Green);
                form.SetStatusInfoLabel("����������� ���������� �� ���������� � ���������� ������������.");
            }
            else if (pcontverres == ContentVerificationResults.contverresModified)
            {
                form.SetSignerNameLabel(certSubjectName);
                form.SetSignStatusLabel("������� ���������������.", System.Drawing.Color.Red);
                form.SetStatusInfoLabel("���� ������� ���� ����������� ���������� ���� ��������.");
                if (m_errors != null)
                    form.SetStatusInfoLabel(m_errors.ToString());
            }
            else if (pcontverres == ContentVerificationResults.contverresUnverified)
            {
                form.SetSignerNameLabel(certSubjectName);
                form.SetSignStatusLabel("������������� �������.", System.Drawing.Color.Red);
                form.SetStatusInfoLabel("�� ������� ��������� �������.");
            }
            else if (pcontverres== ContentVerificationResults.contverresValid && pcertverres == CertificateVerificationResults.certverresError)
            {
                form.SetSignerNameLabel(certSubjectName);
                form.SetSignStatusLabel("������� ���������������.", System.Drawing.Color.Red);
                form.SetStatusInfoLabel("�� ������� ��������� ����������� �����������.");
                if (m_errors != null)
                    form.SetStatusInfoLabel(m_errors.ToString());
            }

            if (form.ShowDialog() == DialogResult.Cancel)
            {
                form.Close();
                form.Dispose();
            }
        }

        public void ShowSignatureSetup(object ParentWindow, SignatureSetup psigsetup)
        {
            throw new NotImplementedException();
        }

        public void ShowSigningCeremony(object ParentWindow, SignatureSetup psigsetup, SignatureInfo psiginfo)
        {
            m_cert = Helper.SelectCertificate(StoreName.My, StoreLocation.CurrentUser);
            if (m_cert == null)
            {
                throw new System.Runtime.InteropServices.COMException("Cancelled", -2147467260 /*E_ABORT*/);
            }
        }

        public void SignXmlDsig(object QueryContinue, SignatureSetup psigsetup, SignatureInfo psiginfo, object XmlDsigStream)
        {
            
            using (COMStream csw = new COMStream(XmlDsigStream))
            {
                byte[] buffer = new byte[csw.Length];
                csw.Read(buffer, 0, (int)csw.Length);

                string xmlSignature = Encoding.UTF8.GetString(buffer);

                //System.IO.File.WriteAllBytes("C:\\work\\bin\\bufNEW4.xml", Encoding.UTF8.GetBytes(xmlSignature));

                if (officeApp.AppVersion == "15.0")
                {
                    int startCert = xmlSignature.IndexOf("<X509Certificate>") + 17;
                    int endCert = xmlSignature.IndexOf("</X509Certificate>");
                    xmlSignature = xmlSignature.Remove(startCert, endCert - startCert);
                    //MessageBox.Show(xmlSignature);

                    xmlSignature = xmlSignature.Insert(startCert, Convert.ToBase64String(m_cert.GetRawCertData()));

                    //MessageBox.Show(xmlSignature);
                }
                else
                {
                    StringBuilder keyInfo = new StringBuilder();
                    keyInfo.Append("<KeyInfo>");
                    keyInfo.Append("<X509Data>");
                    keyInfo.Append("<X509Certificate>");

                    keyInfo.Append(Convert.ToBase64String(m_cert.GetRawCertData()));

                    keyInfo.Append("</X509Certificate>");
                    keyInfo.Append("</X509Data>");
                    keyInfo.Append("</KeyInfo>");
                    xmlSignature = xmlSignature.Replace("<KeyInfo/>", keyInfo.ToString());
                }


                if (officeApp.AppVersion == "14.0" || officeApp.AppVersion == "15.0")
                {
                    int startObj = xmlSignature.IndexOf("<Object>");
                    int endObj = xmlSignature.IndexOf("</Signature>");
                    xmlSignature = xmlSignature.Remove(startObj, endObj - startObj);
                }

                if (officeApp.AppVersion == "12.0")
                {
                    int start = xmlSignature.IndexOf("<SignatureMethod");
                    xmlSignature = xmlSignature.Insert(start + 16, " Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"");
                }

                xmlSignature = xmlSignature.Replace("\r\n", string.Empty).Replace("\n", string.Empty).Replace("\r", string.Empty);

                int hashLength = 0;
                int objLength = 0;
                string hashAlgorithm = "belt-hash256";
                int res = Helper.GetHashAndObjectLength(hashAlgorithm, "", ref objLength, ref hashLength);
                if (res != 0)
                {
                    throw new System.Runtime.InteropServices.COMException("Cancelled", -2147467260 /*E_ABORT*/);
                }

                byte[] hashObj = new byte[objLength];
                byte[] hash = new byte[hashLength];
                res = Helper.CalculateHash(hashAlgorithm, "", hashObj, Encoding.UTF8.GetBytes(xmlSignature), ref hash);
                if (res != 0)
                {
                    throw new System.Runtime.InteropServices.COMException("Cancelled", -2147467260 /*E_ABORT*/);
                }

                CRYPT_KEY_PROV_INFO ctx = Helper.AcquirePrivateKeyName(m_cert);
                int cbResult = 0;
                byte[] signatureValue = null;
                res = Helper.SignHash(ctx.pwszProvName, ctx.pwszContainerName, hash, ref signatureValue, ref cbResult, 0);
                if (res != 0)
                {
                    throw new System.Runtime.InteropServices.COMException("Cancelled", -2147467260 /*E_ABORT*/);
                }

                //signatureValue = new byte[cbResult];
                //res = Helper.SignHash(ctx.pwszProvName, ctx.pwszContainerName, hash, ref signatureValue, ref cbResult, 0);
                //if (res != 0)
                //{
                //    throw new System.Runtime.InteropServices.COMException("Cancelled", -2147467260 /*E_ABORT*/);
                //}

                string xmlSignatureValue = "<SignatureValue>" + Convert.ToBase64String(signatureValue) + "</SignatureValue>";
                xmlSignature = xmlSignature.Replace("<SignatureValue/>", xmlSignatureValue);
                //System.IO.File.WriteAllBytes("C:\\work\\bin\\bufNEW1.xml", bData);
                //MessageBox.Show(xmlSignature);
                byte[] outBuf = new byte[xmlSignature.Length];
                outBuf = Encoding.UTF8.GetBytes(xmlSignature);
                csw.SetLength(0);
                csw.Position = 0;
                csw.Write(outBuf, 0, xmlSignature.Length);
            }
        }

        public void VerifyXmlDsig(object QueryContinue, SignatureSetup psigsetup, SignatureInfo psiginfo, object XmlDsigStream, ref ContentVerificationResults pcontverres, ref CertificateVerificationResults pcertverres)
        {
            using (COMStream csw = new COMStream(XmlDsigStream))
            {
                byte[] buffer = new byte[csw.Length];
                csw.Read(buffer, 0, (int)csw.Length);

                pcontverres = ContentVerificationResults.contverresUnverified;
                pcertverres = CertificateVerificationResults.certverresUnverified;

                string xmlSignature = Encoding.UTF8.GetString(buffer);
                //System.IO.File.WriteAllBytes("C:\\work\\bin\\bufNEW2.xml",Encoding.UTF8.GetBytes(xmlSignature));
                int signLength = GetSignatureLength(xmlSignature);

                byte[] bSignatureValue = new byte[signLength];
                bSignatureValue = GetSignatureValue(ref xmlSignature, signLength);

                byte[] bCert = new byte[GetCertificateValue(xmlSignature).Length];
                bCert = GetCertificateValue(xmlSignature);
                xmlSignature = xmlSignature.Replace("\r\n", string.Empty).Replace("\n", string.Empty).Replace("\r", string.Empty);

                if (m_cert == null)
                {
                    m_cert = new X509Certificate2();
                }
                m_cert.Import(bCert);

                uint magic = 0;
                string algorithm = null;

                if(m_cert.GetKeyAlgorithm().Equals("1.2.112.0.2.0.34.101.45.2.1"))
                {
                    algorithm = "bign-pubkey";
                    if(m_cert.GetKeyAlgorithmParametersString().Equals("060A2A7000020022652D0301"))
                    {
                        magic = CNGWrapper.BIGN_PUBLIC_CURVE256_MAGIC;
                    }
                    else if(m_cert.GetKeyAlgorithmParametersString().Equals("060A2A7000020022652D0302"))
                    {
                        magic = CNGWrapper.BIGN_PUBLIC_CURVE384_MAGIC;
                    }
                    else if(m_cert.GetKeyAlgorithmParametersString().Equals("060A2A7000020022652D0303"))
                    {
                        magic = CNGWrapper.BIGN_PUBLIC_CURVE512_MAGIC;
                    }
                    else
                    {
                        throw new System.Runtime.InteropServices.COMException("Cancelled", -2147467260 /*E_ABORT*/);
                    }
                }
                else if(m_cert.GetKeyAlgorithm().Equals("1.2.112.0.2.0.1176.2.2.1"))
                {
                    algorithm = "stb1176-pubkey";
                    if (m_cert.GetKeyAlgorithmParametersString().Equals("060B2A70000200891802030301"))
                    {
                        magic = CNGWrapper.STB1176_PUBLIC_PARAM3_MAGIC;
                    }
                    else if (m_cert.GetKeyAlgorithmParametersString().Equals("060B2A70000200891802030601"))
                    {
                        magic = CNGWrapper.STB1176_PUBLIC_PARAM6_MAGIC;
                    }
                    else if (m_cert.GetKeyAlgorithmParametersString().Equals("060B2A70000200891802030A01"))
                    {
                        magic = CNGWrapper.STB1176_PUBLIC_PARAM10_MAGIC;
                    }
                    else
                    {
                        throw new System.Runtime.InteropServices.COMException("Cancelled", -2147467260 /*E_ABORT*/);
                    }
                }
                else if(m_cert.GetKeyAlgorithm().Equals("1.2.112.0.2.0.1176.2.2.2"))
                {
                    algorithm = "stb1176pre-pubkey";
                    if (m_cert.GetKeyAlgorithmParametersString().Equals("060B2A70000200891802030301"))
                    {
                        magic = CNGWrapper.STB1176_PRE_PUBLIC_PARAM3_MAGIC;
                    }
                    else if (m_cert.GetKeyAlgorithmParametersString().Equals("060B2A70000200891802030601"))
                    {
                        magic = CNGWrapper.STB1176_PRE_PUBLIC_PARAM6_MAGIC;
                    }
                    else if (m_cert.GetKeyAlgorithmParametersString().Equals("060B2A70000200891802030A01"))
                    {
                        magic = CNGWrapper.STB1176_PRE_PUBLIC_PARAM10_MAGIC;
                    }
                    else
                    {
                        throw new System.Runtime.InteropServices.COMException("Cancelled", -2147467260 /*E_ABORT*/);
                    }
                }
                else
                {
                    throw new System.Runtime.InteropServices.COMException("Cancelled", -2147467260 /*E_ABORT*/);
                }

                int hashLength = 0;
                int objLength = 0;
                string hashAlgorithm = "belt-hash256";
                int res = Helper.GetHashAndObjectLength(hashAlgorithm, "", ref objLength, ref hashLength);
                if (res != 0)
                {
                    throw new System.Runtime.InteropServices.COMException("Cancelled", -2147467260 /*E_ABORT*/);
                }

                byte[] hashObj = new byte[objLength];
                byte[] hash = new byte[hashLength];
                res = Helper.CalculateHash(hashAlgorithm, "", hashObj, Encoding.UTF8.GetBytes(xmlSignature), ref hash);
                if (res != 0)
                {
                    throw new System.Runtime.InteropServices.COMException("Cancelled", -2147467260 /*E_ABORT*/);
                }

                res = Helper.VerifySignature(algorithm, m_cert.GetPublicKey(), magic, hash, bSignatureValue);
                if (res != 0)
                {
                    throw new System.Runtime.InteropServices.COMException("Cancelled", -2147467260 /*E_ABORT*/);
                }

                if (res != 0)
                {
                    pcontverres = ContentVerificationResults.contverresModified;                    
                }
                else
                {
                    pcontverres = ContentVerificationResults.contverresValid;
                }

                bool chainIsValid = false;

                var chain = new X509Chain();
                chain.ChainPolicy.RevocationFlag = X509RevocationFlag.ExcludeRoot;
                chain.ChainPolicy.RevocationMode = X509RevocationMode.Offline;
                //chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                //chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;


                chainIsValid = chain.Build(m_cert);



                if (chainIsValid == true)
                {
                    pcertverres = CertificateVerificationResults.certverresValid;
                }
                else
                {
                    m_errors = new StringBuilder();
                    m_errors.Append("\r\n");
                    int i = 1;

                    foreach (X509ChainStatus st in chain.ChainStatus)
                    {
                        switch (st.Status)
                        {
                            case X509ChainStatusFlags.UntrustedRoot:
                                {
                                    m_errors.Append(i.ToString() + ". ��� ������� � ��������� �����������.\r\n");
                                    break;
                                }
                            case X509ChainStatusFlags.NotTimeValid:
                                {
                                    m_errors.Append(i.ToString() + ". ���� �������� ����������� ����.\r\n");
                                    break;
                                }
                            case X509ChainStatusFlags.NotSignatureValid:
                                {
                                    m_errors.Append(i.ToString() + ". ������� ����������� ���������������.\r\n");
                                    break;
                                }
                            case X509ChainStatusFlags.Cyclic:
                                {
                                    m_errors.Append(i.ToString() + ". ������� ������������ �� ����� ���� ���������.\r\n");
                                    break;
                                }
                            case X509ChainStatusFlags.RevocationStatusUnknown:
                                {
                                    m_errors.Append(i.ToString() + ". ���������� ���������� ��� �� ������� ����������.\r\n");
                                    break;
                                }
                            case X509ChainStatusFlags.Revoked:
                                {
                                    m_errors.Append(i.ToString() + ". ���������� ��� �������.\r\n");
                                    break;
                                }
                            case X509ChainStatusFlags.PartialChain:
                                {
                                    m_errors.Append(i.ToString() + ". ������� �� ����� ���� ��������� �� ��������� �����������.\r\n");
                                    break;
                                }
                            case X509ChainStatusFlags.OfflineRevocation:
                                {
                                    m_errors.Append(i.ToString() + ". �� ������ ��������� ������ ���������� ������������.\r\n");
                                    break;
                                }
                            case X509ChainStatusFlags.NotValidForUsage:
                                {
                                    m_errors.Append(i.ToString() + ". ������������� ����� �����������. \r\n");
                                    break;
                                }
                            case X509ChainStatusFlags.InvalidBasicConstraints:
                                {
                                    m_errors.Append(i.ToString() + ". ������� ������������ �������� ������������ ��-�� ������������ �������� �����������. \r\n");
                                    break;
                                }
                            case X509ChainStatusFlags.InvalidExtension:
                                {
                                    m_errors.Append(i.ToString() + ". ������� ������������ �������� ������������ ��-�� ������������� ����������.\r\n");
                                    break;
                                }
                            case X509ChainStatusFlags.InvalidNameConstraints:
                                {
                                    m_errors.Append(i.ToString() + ". ������� ������������� �������� ������������ ��-�� ������������ ����������� �����. \r\n");
                                    break;
                                }
                            case X509ChainStatusFlags.InvalidPolicyConstraints:
                                {
                                    m_errors.Append(i.ToString() + ". ������� ������������� �������� ������������ ��-�� ������������ ����������� ��������. \r\n");
                                    break;
                                }
                        }
                        i++;
                    }
                    pcertverres = CertificateVerificationResults.certverresError;
                }
            }
        }

        public string GetCustomUI(string RibbonID)
        {
            Microsoft.Office.Interop.Word.Application WordApp = null;
            try
            {
                WordApp = (Microsoft.Office.Interop.Word.Application)applicationObject;
            }
            catch (Exception)
            {
            }

            if (WordApp != null)
            {
                officeApp.AppType = "Word";
                officeApp.AppVersion = WordApp.Version;
            }

            Microsoft.Office.Interop.Excel.Application ExcelApp = null;

            try
            {
                ExcelApp = (Microsoft.Office.Interop.Excel.Application)applicationObject;
            }
            catch (Exception)
            {
            }

            if (ExcelApp != null)
            {
                officeApp.AppType = "Excel";
                officeApp.AppVersion = ExcelApp.Version;
            }

            Microsoft.Office.Interop.PowerPoint.Application PPApp = null;

            try
            {
                PPApp = (Microsoft.Office.Interop.PowerPoint.Application)applicationObject;
            }
            catch (Exception)
            {
            }

            if (PPApp != null)
            {
                officeApp.AppType = "PowerPoint";
                officeApp.AppVersion = PPApp.Version;
            }


            if (officeApp.AppVersion == "14.0" || officeApp.AppVersion == "15.0")
                return GetResource("CustomUI2010.xml");
            else if (officeApp.AppVersion == "12.0")
                return GetResource("customUI2007.xml");
            else
                return null;
        }

        private string GetResource(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            foreach (string name in asm.GetManifestResourceNames())
            {
                if (name.EndsWith(resourceName))
                {
                    System.IO.TextReader tr = new System.IO.StreamReader(asm.GetManifestResourceStream(name));
                    //Debug.Assert(tr != null); 
                    string resource = tr.ReadToEnd();
                    tr.Close();
                    return resource;
                }
            }
            return null;
        }

        public void btnAddSignature_Clicked(IRibbonControl control)
        {
            if (officeApp.AppType == "Word")
            {
                try
                {
                    Microsoft.Office.Interop.Word.Application WordApp = (Microsoft.Office.Interop.Word.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Word.Application");
                    Microsoft.Office.Interop.Word.Document doc = WordApp.Documents.Application.ActiveDocument;
                    doc.Signatures.AddNonVisibleSignature("{EA962C80-D41D-4382-A600-894FA93DE73F}");
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else if (officeApp.AppType == "Excel")
            {
                Microsoft.Office.Interop.Excel.Application ExcelApp = (Microsoft.Office.Interop.Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
                Microsoft.Office.Interop.Excel.Workbook doc = ExcelApp.Workbooks.Application.ActiveWorkbook;
                doc.Signatures.AddNonVisibleSignature("{EA962C80-D41D-4382-A600-894FA93DE73F}");
            }
            else if (officeApp.AppType == "PowerPoint")
            {
                Microsoft.Office.Interop.PowerPoint.Application PPApp = (Microsoft.Office.Interop.PowerPoint.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Powerpoint.Application");
                Microsoft.Office.Interop.PowerPoint.Presentation doc = PPApp.Presentations.Application.ActivePresentation;
                doc.Signatures.AddNonVisibleSignature("{EA962C80-D41D-4382-A600-894FA93DE73F}");
            }
            else
            {
                return;
            }
        }

        private int GetSignatureLength(string content)
        {
            int start = content.IndexOf("<SignatureValue>") + 16;
            int end = content.IndexOf("</SignatureValue>");
            return Convert.FromBase64String(content.Substring(start, end - start)).Length;
        }

        private byte[] GetSignatureValue(ref string content, int signLength)
        {
            int start = content.IndexOf("<SignatureValue>") + 16;
            int end = content.IndexOf("</SignatureValue>");
            string xmlSignatureValue = content.Substring(start, end - start);
            content = content.Remove(start, end - start + 17);
            content = content.Replace("<SignatureValue>", "<SignatureValue/>");
            byte[] value = new byte[signLength];
            value = Convert.FromBase64String(xmlSignatureValue);
            return value;
        }

        private byte[] GetCertificateValue(string content)
        {
            int start = content.IndexOf("<X509Certificate>") + 17;
            int end = content.IndexOf("</X509Certificate>");
            byte[] value = new byte[end - start];
            value = Convert.FromBase64String(content.Substring(start, end - start));
            return value;
        }

        private void form_ViewCertClicked()
        {
            m_sigInfo.ShowSignatureCertificate(m_parentWindow);
        }

        private void form_AdditionalInfoClicked()
        {
            DateTime sysDT = m_sigInfo.GetSignatureDetail(SignatureDetail.sigdetLocalSigningTime);
            string winVer = m_sigInfo.GetSignatureDetail(SignatureDetail.sigdetWindowsVersion);
            string officeVer = m_sigInfo.GetSignatureDetail(SignatureDetail.sigdetOfficeVersion);
            UInt32 num = m_sigInfo.GetSignatureDetail(SignatureDetail.sigdetNumberOfMonitors);
            UInt32 monHeight = m_sigInfo.GetSignatureDetail(SignatureDetail.sigdetVertResolution);
            UInt32 monWidth = m_sigInfo.GetSignatureDetail(SignatureDetail.sigdetHorizResolution);
            UInt32 monColor = m_sigInfo.GetSignatureDetail(SignatureDetail.sigdetColorDepth);
            StringBuilder sb = new StringBuilder();
            sb.Append("��� ������� �������� ��������� �������������� ��������: \r\n");
            sb.Append("��������� ���� � �����: \t" + sysDT.ToString() + "\n");
            sb.Append("������ Windows: \t\t" + winVer + "\n");
            sb.Append("������ Office: \t\t" + officeVer + "\n");
            sb.Append("����� ���������: \t\t" + num.ToString() + "\n");
            sb.Append("�������� �������: \t" + monWidth.ToString() + " ��� � " + monHeight.ToString() + " ��� - " + monColor.ToString() + " ���/���");
            MessageBox.Show(sb.ToString(), "�������������� ��������", MessageBoxButtons.OK);
        }
    }
}