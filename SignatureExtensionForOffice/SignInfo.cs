﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SignatureExtensionForOffice
{
    public partial class SignInfo : Form
    {
        #region Constructor

        public SignInfo()
        {
            InitializeComponent();
        }

        #endregion

        #region Events

        public delegate void Handler();

        public event Handler ViewCertClicked;

        public event Handler AdditionalInfoClicked;

        #endregion

        public void SetSignStatusLabel(string text, Color color)
        {
            lbSignStatus.Text = text;
            lbSignStatus.ForeColor = color;
        }

        public void SetStatusInfoLabel(string text)
        {
            tbStatusInfo.AppendText(text);
        }

        public void SetSignerNameLabel(string text)
        {
            if (text != string.Empty)
                lbSignerName.Text = "Имя: "+text;
            else
                lbSignerName.Text = "Имя: Неизвестный подписавший";
        }

        public void SetDateLabel(string text)
        {
            lbDate.Text = "Дата подписания: "+text;
        }

        private void btnViewCertificate_Click(object sender, EventArgs e)
        {
            if (ViewCertClicked != null)
            {
                ViewCertClicked();
            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnAddInfo_Click(object sender, EventArgs e)
        {
            if (AdditionalInfoClicked != null)
            {
                AdditionalInfoClicked();
            }
        }


    }
} 
