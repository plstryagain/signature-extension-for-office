﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.InteropServices;

namespace SignatureExtensionForOffice
{
    class Helper
    {
        public static X509Certificate2 SelectCertificate(StoreName storeName, StoreLocation storeLocation)
        {
            X509Certificate2 cert;
            var store = new X509Store(storeName, storeLocation);
            store.Open(OpenFlags.ReadOnly);

            try
            {
                X509Certificate2Collection collection;
                collection = X509Certificate2UI.SelectFromCollection(store.Certificates, "Выберите сертификат",
                                                                    "Выберите сертификат для подписи",
                                                                     X509SelectionFlag.SingleSelection);

                if (collection.Count != 1)
                {
                    cert = null;
                }
                else
                {
                    cert = collection[0];
                }
            }
            finally
            {
                store.Close();
            }

            return cert;
        }

        public static int GetHashAndObjectLength(string algorithm, string implementation, ref int objLength, ref int hashLength)
        {
            int res = 0;
            IntPtr hAlg = IntPtr.Zero;

            try
            {
                res = CNGWrapper.BCryptOpenAlgorithmProvider(out hAlg, algorithm, implementation, 0);
                if(res != 0)
                {
                    return res;
                }

                byte[] pbOut = new byte[]{0, 0, 0, 0};
                int cbResult = 0;
                res = CNGWrapper.BCryptGetProperty(hAlg, CNGWrapper.BCRYPT_HASH_LENGTH, pbOut, pbOut.Length, ref cbResult, 0);
                if (res != 0)
                {
                    return res;
                }


                hashLength = BitConverter.ToInt32(pbOut, 0);

                res = CNGWrapper.BCryptGetProperty(hAlg, CNGWrapper.BCRYPT_OBJECT_LENGTH, pbOut, pbOut.Length, ref cbResult, 0);
                if (res != 0)
                {
                    return res;
                }

                objLength = BitConverter.ToInt32(pbOut, 0);
            }
            finally
            {
                if(hAlg != IntPtr.Zero)
                {
                    CNGWrapper.BCryptCloseAlgorithmProvider(hAlg, 0);
                }
            }

            return res;
        }

        public static int CalculateHash(string algorithm, string implementation, byte[] hashObj, byte[] inputData, ref byte[] outputData)
        {
            int res = 0;
            IntPtr hAlg = IntPtr.Zero;
            IntPtr hHash = IntPtr.Zero;

            try
            {
                res = CNGWrapper.BCryptOpenAlgorithmProvider(out hAlg, algorithm, implementation, 0);
                if (res != 0)
                {
                    return res;
                }

                res = CNGWrapper.BCryptCreateHash(hAlg, out hHash, hashObj, hashObj.Length, null, 0, 0);
                if (res != 0)
                {
                    return res;
                }

                res = CNGWrapper.BCryptHashData(hHash, inputData, inputData.Length, 0);
                if (res != 0)
                {
                    return res;
                }

                res = CNGWrapper.BCryptFinishHash(hHash, outputData, outputData.Length, 0);
                if (res != 0)
                {
                    return res;
                }
            }
            finally
            {
                if(hHash != IntPtr.Zero)
                {
                    CNGWrapper.BCryptDestroyHash(hHash);
                }

                if (hAlg != IntPtr.Zero)
                {
                    CNGWrapper.BCryptCloseAlgorithmProvider(hAlg, 0);
                }
            }

            return res;
        }

        public static CRYPT_KEY_PROV_INFO AcquirePrivateKeyName(X509Certificate2 cert)
        {
            IntPtr pCertContext = cert.Handle;
            uint pcbData = 0;

            try
            {
                if (CNGWrapper.CertGetCertificateContextProperty(pCertContext, 2, IntPtr.Zero, ref pcbData))
                {
                    IntPtr pbData = Marshal.AllocHGlobal((int)pcbData);
                    try
                    {
                        if (CNGWrapper.CertGetCertificateContextProperty(pCertContext, 2, pbData, ref pcbData))
                        {
                            CRYPT_KEY_PROV_INFO ctx = (CRYPT_KEY_PROV_INFO)Marshal.PtrToStructure(pbData, typeof(CRYPT_KEY_PROV_INFO));
                            return ctx;
                        }
                        else
                        {
                            throw new System.Runtime.InteropServices.COMException("Cancelled", -2147467260 /*E_ABORT*/);
                        }
                    }
                    finally
                    {
                        Marshal.FreeHGlobal(pbData);
                    }
                }
            }
            finally
            {

            }
            throw new System.Runtime.InteropServices.COMException("Cancelled", -2147467260 /*E_ABORT*/);
        }

        public static int SignHash(string providerName, string keyName, byte[] hashValue, ref byte[] signatureValue, ref int pcbResult, int flags)
        {
            IntPtr hProv = IntPtr.Zero;
            IntPtr hKey = IntPtr.Zero;

            try
            {
                int res = 0;

                res = CNGWrapper.NCryptOpenStorageProvider(out hProv, providerName, flags);
                if(res != 0)
                {
                    return res;
                }

                res = CNGWrapper.NCryptOpenKey(hProv, out hKey, keyName, 0, flags);
                if(res != 0)
                {
                    return res;
                }

               
                res = CNGWrapper.NCryptSignHash(hKey,
                                       IntPtr.Zero,
                                       hashValue,
                                       hashValue.Length,
                                       null,
                                       0,
                                       ref pcbResult,
                                       0);

                signatureValue = new byte[pcbResult];

                res = CNGWrapper.NCryptSignHash(hKey,
                                   IntPtr.Zero,
                                   hashValue,
                                   hashValue.Length,
                                   signatureValue,
                                   signatureValue.Length,
                                   ref pcbResult,
                                   0);

                return res;
            }
            finally
            {
                if(hKey != null)
                {
                    CNGWrapper.NCryptFreeObject(hKey);
                }

                if(hProv != null)
                {
                    CNGWrapper.NCryptFreeObject(hProv);
                }
            }
        }

        public static int VerifySignature(string algorithm, byte[] pubKeyValue, uint magic, byte[] hashValue, byte[] signatureValue)
        {
            IntPtr hAlg = IntPtr.Zero;
            IntPtr hKey = IntPtr.Zero;
            try
            {
                int res = CNGWrapper.BCryptOpenAlgorithmProvider(out hAlg, algorithm, "", 0);
                if (res != 0)
                {
                    return res;
                }

                uint pub_key_len = (uint)pubKeyValue.Length;
                uint priv_key_len = pub_key_len / 2;
                byte[] blob = new byte[pub_key_len + 8];

                blob[0] = (byte)magic;
                blob[1] = (byte)(magic >> 0x08);
                blob[2] = (byte)(magic >> 0x10);
                blob[3] = (byte)(magic >> 0x18);

                blob[4] = (byte)priv_key_len;
                blob[5] = (byte)(priv_key_len >> 0x08);
                blob[6] = (byte)(priv_key_len >> 0x10);
                blob[7] = (byte)(priv_key_len >> 0x18);

                Array.Copy(pubKeyValue, 0, blob, 8, pub_key_len);

                res = CNGWrapper.BCryptImportKeyPair(hAlg, 
                    IntPtr.Zero, 
                    CNGWrapper.BCRYPT_ECCPUBLIC_BLOB, 
                    out hKey, 
                    blob, 
                    blob.Length, 
                    0);
                if (res != 0)
                {
                    return res;
                }

                res = CNGWrapper.BCryptVerifySignature(hKey, 
                    IntPtr.Zero, 
                    hashValue, 
                    hashValue.Length, 
                    signatureValue, 
                    signatureValue.Length, 
                    0);
                uint res2 = (uint)res;
                return res;
            }
            finally
            {
                if(hKey != null)
                {
                    CNGWrapper.BCryptDestroyKey(hKey);
                }

                if(hAlg != null)
                {
                    CNGWrapper.BCryptCloseAlgorithmProvider(hAlg, 0);
                }
            }
        }

        public static string ByteArrayToHexString(byte[] Bytes)
        {
            StringBuilder Result = new StringBuilder(Bytes.Length * 2);
            string HexAlphabet = "0123456789ABCDEF";

            foreach (byte B in Bytes)
            {
                Result.Append(HexAlphabet[(int)(B >> 4)]);
                Result.Append(HexAlphabet[(int)(B & 0xF)]);
            }

            return Result.ToString();
        }
    }
}
