﻿using System;
using iop = System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using COMIStream = System.Runtime.InteropServices.ComTypes.IStream;
using System.IO;

namespace SignatureExtensionForOffice
{
    //public class ComStreamWrapper : System.IO.Stream
    //{
    //    private IStream mSource;
    //    private IntPtr mInt64;

    //    public ComStreamWrapper(IStream source)
    //    {
    //        mSource = source;
    //        mInt64 = iop.Marshal.AllocCoTaskMem(8);
    //    }

    //    ~ComStreamWrapper()
    //    {
    //        iop.Marshal.FreeCoTaskMem(mInt64);
    //    }

    //    public override bool CanRead { get { return true; } }
    //    public override bool CanSeek { get { return true; } }
    //    public override bool CanWrite { get { return true; } }

    //    public override void Flush()
    //    {
    //        mSource.Commit(0);
    //    }

    //    public override long Length
    //    {
    //        get
    //        {
    //            STATSTG stat;
    //            mSource.Stat(out stat, 1);
    //            return stat.cbSize;
    //        }
    //    }

    //    public override long Position
    //    {
    //        get { throw new NotImplementedException(); }
    //        set { throw new NotImplementedException(); }
    //    }

    //    public override int Read(byte[] buffer, int offset, int count)
    //    {
    //        if (offset != 0) throw new NotImplementedException();
    //        mSource.Read(buffer, count, mInt64);
    //        return iop.Marshal.ReadInt32(mInt64);
    //    }

    //    public override long Seek(long offset, System.IO.SeekOrigin origin)
    //    {
    //        mSource.Seek(offset, (int)origin, mInt64);
    //        return iop.Marshal.ReadInt64(mInt64);
    //    }

    //    public override void SetLength(long value)
    //    {
    //        mSource.SetSize(value);
    //    }

    //    public override void Write(byte[] buffer, int offset, int count)
    //    {
    //        if (offset != 0) throw new NotImplementedException();
    //        mSource.Write(buffer, count, IntPtr.Zero);
    //    }
    //}

    public class COMStream : Stream, IDisposable
    {
        #region Constants

        public enum STATFLAG
        {
            STATFLAG_DEFAULT = 0x00000000,
            STATFLAG_NONAME = 0x00000001,
        }

        public enum STGC
        {
            STGC_DEFAULT = 0x00000000,
            STGC_OVERWRITE = 0x00000000,
            STGC_ONLYIFCURRENT = 0x00000000,
            STGC_DANGEROUSLYCOMMITMERELYTODISKCACHE = 0x00000000,
            STGC_CONSOLIDATE = 0x00000000,
        }

        [Flags]
        public enum STGM
        {
            // Access
            STGM_READ = 0x00000000,
            STGM_WRITE = 0x00000001,
            STGM_READWRITE = 0x00000002,

            // Sharing
            STGM_SHARE_DENY_NONE = 0x00000040,
            STGM_SHARE_DENY_READ = 0x00000030,
            STGM_SHARE_DENY_WRITE = 0x00000020,
            STGM_SHARE_EXCLUSIVE = 0x00000010,
            STGM_PRIORITY = 0x00040000,

            // Creation
            STGM_CREATE = 0x00001000,
            STGM_CONVERT = 0x00020000,
            STGM_FAILIFTHERE = 0x00000000,

            // Transactioning
            STGM_DIRECT = 0x00000000,
            STGM_TRANSACTED = 0x00010000,

            // Transactioning Performance
            STGM_NOSCRATCH = 0x00100000,
            STGM_NOSNAPSHOT = 0x00200000,

            // Direct SWMR and Simple
            STGM_SIMPLE = 0x08000000,
            STGM_DIRECT_SWMR = 0x00400000,

            // Delete On Release
            STGM_DELETEONRELEASE = 0x04000000,
        }

        public enum STREAM_SEEK
        {
            STREAM_SEEK_SET = 0x00000000,
            STREAM_SEEK_CUR = 0x00000001,
            STREAM_SEEK_END = 0x00000002,
        }

        #endregion Constants

        #region Members

        private COMIStream istream;

        #endregion Members

        #region Constructors

        public COMStream(object stream)
            : this(stream as COMIStream)
        {
        }

        public COMStream(COMIStream istream)
        {
            this.istream = istream;
            if (this.istream == null)
                throw new ArgumentNullException();
        }

        #endregion Constructors

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            this.istream = null;
        }

        private void CheckDisposed()
        {
            if (this.istream == null)
                throw new ObjectDisposedException("COMStream");
        }

        #endregion IDisposable

        #region Stream methods

        public override void Flush()
        {
            this.CheckDisposed();

            this.istream.Commit((int)STGC.STGC_DEFAULT);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            this.CheckDisposed();

            STREAM_SEEK seek;
            switch (origin)
            {
                case SeekOrigin.Begin:
                    seek = STREAM_SEEK.STREAM_SEEK_SET;
                    break;
                case SeekOrigin.Current:
                    seek = STREAM_SEEK.STREAM_SEEK_CUR;
                    break;
                case SeekOrigin.End:
                    seek = STREAM_SEEK.STREAM_SEEK_END;
                    break;

                default:
                    throw new ArgumentException("origin");
            }

            long position = -1;
            unsafe
            {
                this.istream.Seek(offset, (int)seek, new IntPtr(&position));
            }
            return position;
        }

        public override void SetLength(long length)
        {
            this.CheckDisposed();

            this.istream.SetSize(length);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            this.CheckDisposed();

            if (buffer == null)
                throw new ArgumentNullException("buffer");
            if (offset < 0)
                throw new ArgumentException("offset");
            if (count < 0)
                throw new ArgumentException("count");

            byte[] readbuffer = buffer;
            if (offset > 0)
                readbuffer = new byte[count];

            int bytesRead = 0;
            unsafe
            {
                this.istream.Read(readbuffer, count, new IntPtr(&bytesRead));
            }

            if (offset > 0)
                Array.Copy(readbuffer, 0, buffer, offset, bytesRead);
            return bytesRead;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            this.CheckDisposed();

            if (buffer == null)
                throw new ArgumentNullException("buffer");
            if (offset < 0)
                throw new ArgumentException("offset");
            if (count < 0)
                throw new ArgumentException("count");

            byte[] writebuffer = buffer;
            if (offset > 0)
            {
                writebuffer = new byte[count];
                Array.Copy(buffer, offset, writebuffer, 0, count);
            }

            int bytesWritten = 0;
            unsafe
            {
                this.istream.Write(writebuffer, count, new IntPtr(&bytesWritten));
            }
        }

        #endregion Stream methods

        #region Stream properties

        public override bool CanRead
        {
            get
            {
                this.CheckDisposed();

                // assume yes
                return true;
            }
        }

        public override bool CanSeek
        {
            get
            {
                this.CheckDisposed();

                // assume yes
                return true;
            }
        }

        public override bool CanWrite
        {
            get
            {
                this.CheckDisposed();

                // assume yes
                return true;
            }
        }

        public override long Length
        {
            get
            {
                this.CheckDisposed();

                STATSTG statstg;
                this.istream.Stat(out statstg, (int)STATFLAG.STATFLAG_NONAME);
                return statstg.cbSize;
            }
        }

        public override long Position
        {
            get
            {
                return this.Seek(0, SeekOrigin.Current);
            }

            set
            {
                long position = this.Seek(value, SeekOrigin.Begin);
            }
        }

        #endregion Stream properties
    }

}